import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;


/**
 * Created by fredericdabrowski on 25/01/2017.
 */


public class Calculator {

    public static void main(String args[]){
        String expression = "2+2*(3+1)";
        CalculatorLexer lexer = new CalculatorLexer(new ANTLRInputStream(expression));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        CalculatorParser parser = new CalculatorParser(tokens);
        CalculatorParser.ExprContext expr = parser.expr();
        System.out.println(expr.getText()+ " = " + new EvalVisitor().visit(expr));
    }

}
