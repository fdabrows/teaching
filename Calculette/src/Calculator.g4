grammar Calculator;

options
{
  language = Java;
  output = AST;
}

@lexer::header {
//package org.meri.antlr_step_by_step.parsers;
}

@parser::header {
//package org.meri.antlr_step_by_step.parsers;
}

// ***************** lexer rules:
INT : ('0'..'9')+;
WS : [ \t\r\n]+ -> skip;

// ***************** parser rules:
// By default, antlr generates in class CalculatorVisitor and CalculatorBaseVisitor
// the methods visitStart that visits a structure of type StartContext (CalculatorParser.java).
// To distinguish different cases, we can ask it to generate more specific structures and
// visitors methods. this is done below by the directive # op, # parent and #integer which
// generate classes OpContext, parentContext and IntegerContext as well as the visitors
// visitOp, visitParent and visitInteger that we implement in EvalVisitor (inheriting from
// CalculatorBaseVisitor.
// In the same way we can give names to the elements of a phrase to access them easily in the
// code (see EvalVisitor.java). Here we use the names left, op, right, sub and val.
start : expr EOF;
expr :
      left=expr op=('*' | '/') right=expr # op
    | left=expr op=('+' | '-') right=expr # op
    | '(' sub=expr ')'          # parent
    | val=INT                   # integer;