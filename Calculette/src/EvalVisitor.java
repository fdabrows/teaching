/**
 * Created by fredericdabrowski on 26/01/2017.
 */
public class EvalVisitor extends CalculatorBaseVisitor<Integer> {


    @Override public Integer visitStart(CalculatorParser.StartContext ctx) { return visitChildren(ctx); }

    @Override public Integer visitOp(CalculatorParser.OpContext ctx) {
        Integer left = visit(ctx.left);
        Integer right = visit(ctx.right);
        switch(ctx.op.getText().charAt(0)) {
            case '+': return left + right;
            case '-': return left - right;
            case '*': return left * right;
            case '/': return left / right;
            default: throw new IllegalArgumentException("Unknown operator " + ctx.getText());
        }
    }

    @Override public Integer visitParent(CalculatorParser.ParentContext ctx) { return visit(ctx.sub); }

    @Override public Integer visitInteger(CalculatorParser.IntegerContext ctx) { return Integer.valueOf(ctx.getText());}
}
